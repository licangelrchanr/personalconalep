package angelchan.aprendiendosqlite.personalconalep;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Angel Chan on 17/11/2016.
 */

public class PersonalDatabase extends SQLiteOpenHelper{
    //constantes
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="personal.db";
    private static final String TABLE_NAME="personal";
    public  static final String COL_NOMBRE="nombre";
    public  static final String COL_PUESTO="puesto";
    public  static final String COL_DEPARTAMENTO="departamento";
    public  static final String COL_GRADOACADEMICO="gradoacademico";
    public  static final String COL_NUMEROTELEFONO="numerotelefono";


    public PersonalDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE " + TABLE_NAME +
                " (_id INTEGER PRIMARY KEY, "
                + COL_NOMBRE+" TEXT NOT NULL COLLATE UNICODE, "
                + COL_PUESTO+" TEXT NOT NULL, "
                + COL_DEPARTAMENTO+" TEXT NOT NULL, "
                + COL_GRADOACADEMICO+" TEXT NOT NULL, "
                + COL_NUMEROTELEFONO+" TEXT NOT NULL)";

        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(upgradeQuery);
    }
    public static long insertPersonal(Context context, String nombre, String puesto, String departamento, String gradoAcademico, String numeroTelefono){
        SQLiteOpenHelper dbOpenHelper = new PersonalDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();
        ContentValues valorPersonal= new ContentValues();
        valorPersonal.put(COL_NOMBRE,nombre);
        valorPersonal.put(COL_PUESTO,puesto);
        valorPersonal.put(COL_DEPARTAMENTO,departamento);
        valorPersonal.put(COL_GRADOACADEMICO,gradoAcademico);
        valorPersonal.put(COL_NUMEROTELEFONO,numeroTelefono);

        long result=-1L;

        try {

            result= database.insert(TABLE_NAME,null,valorPersonal);
            if (result != -1L){
                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(PersonalsLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

        } finally {
            dbOpenHelper.close();
        }


//        long result = database.insert(TABLE_NAME,null,valorAnimal);
//        dbOpenHelper.close();
        return result;


    }

    public static Cursor devuelveTodos (Context context){
        SQLiteOpenHelper dbOpenHelper = new PersonalDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE,COL_PUESTO, BaseColumns._ID}
                ,null,null,null,null,
                COL_NOMBRE+" ASC");


    }

    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper = new PersonalDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();


        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE,COL_PUESTO,COL_DEPARTAMENTO,COL_GRADOACADEMICO,COL_NUMEROTELEFONO,BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null,
                null,
                COL_NOMBRE +" ASC");

    }

    public static int eliminaConId(Context context, long personalId){
        SQLiteOpenHelper dbOpenHelper = new PersonalDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME,BaseColumns._ID + " =?",
                new String[]{String.valueOf(personalId)});

        if (resultado != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(PersonalsLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static int actualizaPersonal(Context context, String nombre,
                                        String puesto, String departamento, String gradoAcademico, String numeroTelefono, long personalId){
        SQLiteOpenHelper dbOpenHelper = new PersonalDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorPersonal= new ContentValues();
        valorPersonal.put(COL_NOMBRE, nombre);
        valorPersonal.put(COL_PUESTO, puesto);
        valorPersonal.put(COL_DEPARTAMENTO, departamento);
        valorPersonal.put(COL_GRADOACADEMICO, gradoAcademico);
        valorPersonal.put(COL_NUMEROTELEFONO, numeroTelefono);


        int result = database.update(
                TABLE_NAME,
                valorPersonal, BaseColumns._ID + " =?",
                new String[]{String.valueOf(personalId)});

        if (result != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(PersonalsLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return result;
    }





}
