package angelchan.aprendiendosqlite.personalconalep;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Angel Chan on 17/11/2016.
 */

public class MiAplicacion extends Application {
    public void onCreate(){
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
