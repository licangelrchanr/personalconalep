package angelchan.aprendiendosqlite.personalconalep;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetallePersonal extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    // crear variables por conveniencia privadas
    private TextView nombreTextView;
    private TextView puestoTextView;
    private TextView departamentoTextView;
    private TextView gradoAcademicoTextView;
    private TextView numeroTelefonoTextView;
    private long personalId;
    public static final String EXTRA_PERSONAL_ID="personal.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_personal);

        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        puestoTextView=(TextView) findViewById(R.id.puesto_text_view);
        departamentoTextView=(TextView) findViewById(R.id.departamento_text_view);
        gradoAcademicoTextView=(TextView) findViewById(R.id.gradoacademico_text_view);
        numeroTelefonoTextView=(TextView) findViewById(R.id.numerotelefono_text_view);

        Intent intencion = getIntent();

        personalId=intencion.getLongExtra(MainActivity.EXTRA_ID_PERSONAL,-1L);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DeletePersonalTask(DetallePersonal.this,personalId).execute();
                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(DetallePersonal.this,AgregarPersonal.class);
                        intent.putExtra(EXTRA_PERSONAL_ID,personalId);
                        startActivity(intent);
                    }
                }
        );

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new PersonallLoader(this,personalId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_NOMBRE);
            String nombrePersonal=data.getString(nombreIndex);

            int puestoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_PUESTO);
            String puestoPersonal=data.getString(puestoIndex);

            int departamentoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_DEPARTAMENTO);
            String departamentoPersonal=data.getString(departamentoIndex);

            int gradoAcademicoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_GRADOACADEMICO);
            String gradoAcademicoPersonal=data.getString(gradoAcademicoIndex);

            int numeroTelefonoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_NUMEROTELEFONO);
            String numeroTelefonoPersonal=data.getString(numeroTelefonoIndex);

            nombreTextView.setText("NOMBRE: "+nombrePersonal);
            puestoTextView.setText("PUESTO: "+puestoPersonal);
            departamentoTextView.setText("DEPARTAMENTO: "+departamentoPersonal);
            gradoAcademicoTextView.setText("GRADO ACADÉMICO: "+gradoAcademicoPersonal);
            numeroTelefonoTextView.setText("TELÉFONO: "+numeroTelefonoPersonal);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class DeletePersonalTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private Long personalId;

        public DeletePersonalTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            personalId=id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = PersonalDatabase.eliminaConId(appContext, personalId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}

