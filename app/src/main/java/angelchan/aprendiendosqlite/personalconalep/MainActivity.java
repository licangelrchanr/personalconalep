package angelchan.aprendiendosqlite.personalconalep;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
// original
/*public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}*/

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

      public final static String EXTRA_ID_PERSONAL = "PERSONAL.ID_PERSONAL";
      private SimpleCursorAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listaDePersonal = (ListView) findViewById(R.id.activity_main);
        listaDePersonal.setOnItemClickListener(this);

        adaptador = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{PersonalDatabase.COL_NOMBRE},
                new int[]{android.R.id.text1},
                0);

        listaDePersonal.setAdapter(adaptador);

        getSupportLoaderManager().initLoader(0,null,this);


        findViewById(R.id.boton_nuevo_personal).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intento = new Intent(MainActivity.this, AgregarPersonal.class);
                startActivity(intento);
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        String nombreAnimal= (String) parent.getItemAtPosition(position);
//        String descripcionAnimal= arregloDeDescripciones[position];

        Intent intencion = new Intent(this,DetallePersonal.class) ; // iniciar la actividad
        intencion.putExtra(EXTRA_ID_PERSONAL,adaptador.getItemId(position));
        startActivity(intencion);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new PersonalsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adaptador.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adaptador.swapCursor(null);
    }
}
