package angelchan.aprendiendosqlite.personalconalep;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

/*
public class AgregarPersonal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_personal);
    }
}
*/

public class AgregarPersonal extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long personalId;
    private EditText nombreEditText;
    private EditText puestoEditText;
    private EditText departamentoEditText;
    private EditText gradoAcademicoEditText;
    private EditText numeroTelefonoEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_personal);

        nombreEditText =(EditText) findViewById(R.id.nombre_edit_text);
        puestoEditText =(EditText) findViewById(R.id.puesto_edit_text);
        departamentoEditText =(EditText) findViewById(R.id.departamento_edit_text);
        gradoAcademicoEditText =(EditText) findViewById(R.id.gradoAcademico_edit_text);
        numeroTelefonoEditText =(EditText) findViewById(R.id.numeroTelefono_edit_text);

        personalId=getIntent().getLongExtra(DetallePersonal.EXTRA_PERSONAL_ID,-1L);
        if (personalId != -1L){
            getSupportLoaderManager().initLoader(0,null,this);
        }


        findViewById(R.id.boton_agregar).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        String nombrePersonal = nombreEditText.getText().toString();
        String puestoPersonal = puestoEditText.getText().toString();
        String departamentoPersonal = departamentoEditText.getText().toString();
        String gradoAcademicoPersonal = gradoAcademicoEditText.getText().toString();
        String numeroTelefonoPersonal = numeroTelefonoEditText.getText().toString();

        // llamamos al metodo que crea el animal y le pasamos los parametros
        if (nombrePersonal.isEmpty()||puestoPersonal.isEmpty()||departamentoPersonal.isEmpty() ){
            Toast.makeText(this,"!Uno ó mas datos no han sido capturados!...",Toast.LENGTH_SHORT).show();
        }
        else {
            new CreatePersonalTask(this, nombrePersonal, puestoPersonal, departamentoPersonal, gradoAcademicoPersonal, numeroTelefonoPersonal, personalId).execute();
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new PersonallLoader(this,personalId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_NOMBRE);
            String nombrePersonal=data.getString(nombreIndex);

            int puestoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_PUESTO);
            String puestoPersonal=data.getString(puestoIndex);

            int departamentoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_DEPARTAMENTO);
            String departamentoPersonal=data.getString(departamentoIndex);

            int gradoAcademicoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_GRADOACADEMICO);
            String gradoAcademicoPersonal=data.getString(gradoAcademicoIndex);

            int numeroTelefonoIndex=data.getColumnIndexOrThrow(PersonalDatabase.COL_NUMEROTELEFONO);
            String numeroTelefonoPersonal=data.getString(numeroTelefonoIndex);


            nombreEditText.setText(nombrePersonal);
            puestoEditText.setText(puestoPersonal);
            departamentoEditText.setText(departamentoPersonal);
            gradoAcademicoEditText.setText(gradoAcademicoPersonal);
            numeroTelefonoEditText.setText(numeroTelefonoPersonal);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    public static class CreatePersonalTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String personalNombre;
        private String personalPuesto;
        private String personalDepartamento;
        private String personalGradoAcademico;
        private String personalNumeroTelefono;

        private long personalId;


        public CreatePersonalTask(Activity activity, String nombre, String puesto, String departamento, String gradoAcademico, String numeroTelefono,  long personalId){
            weakActivity = new WeakReference<Activity>(activity);
            personalNombre = nombre;
            personalPuesto = puesto;
            personalDepartamento= departamento;
            personalGradoAcademico = gradoAcademico;
            personalNumeroTelefono= numeroTelefono;
            this.personalId = personalId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();
            boolean succes = false;

            if (personalId!=-1L){
                int filasAfectadas=PersonalDatabase.actualizaPersonal(appContext, personalNombre, personalPuesto, personalDepartamento, personalGradoAcademico, personalNumeroTelefono,personalId);
                succes=(filasAfectadas != 0);
            } else {
                long id=PersonalDatabase.insertPersonal(appContext, personalNombre, personalPuesto, personalDepartamento, personalGradoAcademico, personalNumeroTelefono);
                succes=(id != -1L);
            }

            return  succes;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}

